# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Kazakh messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Talgat Daniyarov
# Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2008-2022
# Dauren Sarsenov <daur88@inbox.ru>, 2008, 2009
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@debian.org>, 2004.
#   Sairan Kikkarin <sairan@sci.kz>, 2006
#   KDE Kazakh l10n team, 2006
#   Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2008, 2009, 2010
#   Dauren Sarsenov <daur88@inbox.ru>, 2009
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-retriever@packages.debian.org\n"
"POT-Creation-Date: 2019-09-29 17:12+0000\n"
"PO-Revision-Date: 2022-12-04 17:19+0600\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>\n"
"Language-Team: Kazakh\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../load-cdrom.templates:1001
msgid "Load installer components from installation media"
msgstr "Орнатқыш құрамасын орнату бейнесінен жүктеу"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid "Failed to copy file from installation media. Retry?"
msgstr "Файлды орнату тасушысынан көшіру сәтсіз аяқталды. Қайталау керек пе?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid ""
"There was a problem reading data. Please make sure you have inserted the "
"installation media correctly. If retrying does not work, you should check "
"the integrity of your installation media (there is an associated entry in "
"the main menu for that)."
msgstr ""
"Деректерді оқу кезінде мәселе орын алды. Орнату тасушысы дұрыс салынғанын "
"тексеріңіз. Қайталап көру көмектеспесе, орнату тасушысының бүтіндігін "
"тексеріңіз (бұл үшін басты мәзірде сәйкес әрекет бар)."
