# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of se.po to Northern Saami
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Børre Gaup <boerre@skolelinux.no>, 2006, 2010.
msgid ""
msgstr ""
"Project-Id-Version: se\n"
"Report-Msgid-Bugs-To: cdrom-retriever@packages.debian.org\n"
"POT-Creation-Date: 2019-09-29 17:12+0000\n"
"PO-Revision-Date: 2019-08-28 18:10+0000\n"
"Last-Translator: leela <53352@protonmail.com>\n"
"Language-Team: Northern Sami <i18n-sme@lister.ping.uio.no>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ((n == 2) ? 1 : 2);\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../load-cdrom.templates:1001
#, fuzzy
msgid "Load installer components from installation media"
msgstr "Geavat sajáiduhttinosiid sajáiduhttin-ISO:s"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid "Failed to copy file from installation media. Retry?"
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid ""
"There was a problem reading data. Please make sure you have inserted the "
"installation media correctly. If retrying does not work, you should check "
"the integrity of your installation media (there is an associated entry in "
"the main menu for that)."
msgstr ""
